TeX Beamer Template
---

__This repo has moved!__ 

Find my up to date Beamer template on Gihub. The repo is located [here](https://github.com/drbunsen/drbunsen-beamer). For more information, please see [this blog post](http://www.drbunsen.org/custom-beamer-theme.html). 
